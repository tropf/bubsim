# Akteure
- Produzent·innen
    - Geld
- Nutzer·innen
    - Wenig Geld für ein gutes Produkt
    - in der Simulation: Mindeststandards
- Aktivist·innen
    - wenig Verschwendung
        - hoher Nutzen aus jedem einzelnem Gerät
        - praktisch: hohe Lebensdauer
- ??? (Politik?)
    - (gibt es jemand mit diesem Interesse)
    - Hohe Versorgung (möglichst viele Menschen haben Zugang zu Technik)
    - (B&B stellt diese Forderung in Form einer "demokratischen" Digitalisierung)
- ??? (Forschung?)
    - (gibt es jemand mit diesem Interesse)
    - Viele Entwickelte Modelle

