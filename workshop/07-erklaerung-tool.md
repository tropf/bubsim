# Einführung Tool
- Tool aufmachen
- Szenario `Turing` auswählen
- Hinweis:
    - Es könnte sinnvoll sein die anderen Szenarien gar nicht anzuschauen
    - weil Spoiler
- Lifecycle für einen Laptop durchgehen
    - Entwurf
    - Preis einstellen (speichern !!1elf!)
    - "Nächstes Jahr"
        - nur hier findet tatsächlich Simulation statt
    - Hinweis auf Anzeige der verbleibenden Jahre
        - Jede Runde hat 12 Jahre
    - danach: Auswertung angezeigt
        - die vlt in nem Tab auflassen

- Anmerkungen
    - Der erste Versuch geht schief. Einfach oben neu starten
    - Kein JS
        - Die UI ist langsam
        - und springt manchmal (aufpassen!)
    - Abbrechen: nicht machen
        - weil da werden Simulationsschritte übersprungen

- **jetzt erst**: URL rausgeben
    - `bubsim.tropf.io`
