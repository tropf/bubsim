# B&B Forderungen
- kurz B&B einordnen

> ### Langlebigkeit von Software und Hardware
> \10. Software muss selbstbestimmt nutzbar sein, reparierbar sein und langfristig instand gehalten werden können, so wie es Open-Source-Software bereits verwirklicht. Hersteller müssen daher beispielsweise Sicherheitsupdates für die Hardware-Lebensdauer von Geräten bereitstellen und nach Ende des Supports den Quellcode als Open-Source-Variante freigeben, statt „Software Locks“ einzubauen.
> \11. Elektronische Geräte müssen reparierbar und recyclebar sein – geplante Obsoleszenz darf es nicht geben. Dafür müssen Garantiefristen massiv ausgeweitet werden; Hersteller müssen Ersatzteile, Reparaturwerkzeug und Know-How für alle anbieten und langfristig vorhalten. Dies soll unterstützt werden durch eine stärkere finanzielle Förderung offener Werkstätten bzw. Repair-Cafés und gemeinwohlorientierter Forschung und Produktentwicklung. Öffentliches Forschungsgeld darf es nur für Open-Source-Produkte geben.

## Wichtige Inhalte
- Problematik "Langlebigkeit" wird angesprochen
    - vlt in die Runde Fragen: wie alt sind eure Geräte
    - evtl. Vergleich Haushaltsgeräte
- HW & SW gemeinsam angesprochen
- geplante Obsoleszenz
    - kurz Begriff erklären
    - "Sollbruchstelle"
- Lösungsvorschläge:
    - Ansätze von Open Source nutzen
    - öffentliche Förderung
    - Hersteller in die Pflicht nehmen
        - u.a.: Ausweitung von Garantiefristen

- Aufhänger erläutern
    - Garantiefristen verlängern
- Das wird erster Schritt im Workshop:
    - Probieren, ob lange Garantie hilft

