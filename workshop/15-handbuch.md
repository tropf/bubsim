# Handbuch
Ein kurzes Handbuch zu Auswertungen.

Falls die Teilnehmer·innen nicht so richtig etwas Beitragen, könnte mensch selbst diese Punkte hier einbringen.

## Beobachtungen
- Alle Größen abklappern
    - Verhältnis Gewinn - Umsatz
    - Entwickelte Modelle
    - Anteil Entwicklung/Garantie an Gesamteinnahmen
    - Ein mal: klären, wie Preisanpassung funktionieren
- Verhalten
    - Was war eure Strategie?
    - Habt ihr eure Strategie angepasst?
    - Würdet ihr etwas anders machen?
    - Angebot: selbes Szenario nochmal spielen
- Phänomene
    - Marksättigung
    - Massenmarkt

## Hypothesen
- Mal bei den anderen Szenarien spicken
- Sehr geringe Kosten für Entwicklung
    - Führt zu mehr entwickelten Geräten
    - sonst keine Große Veränderung
- Mehr Akzeptanz für langsame Geräte
    - Parameter: durchschnittliche Minimalperformance
    - führt zu weniger Verkäufen
- Kunden haben viel Geld
    - mehr Gewinn
    - sonst keine Änderung an den Größen
- Mehr Startkapital
    - führt zu höherer Abdeckung
    - weil weniger Gewinn pro Gerät in Kauf genommen werden kann
- Kunden haben wenig Geld
    - weniger Versorgung
    - schlechtere Geräte
