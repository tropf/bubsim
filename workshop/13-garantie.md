# Garantie
Wie Garantie wirklich funktioniert.

- Szenario "Dijkstra"
    - wenig technischer Fortschritt
    - hohe Garantie

## Auswertung
- Wie sieht ein Garantiefall aus?
    - i.d.R. keine Reparatur
    - Finanzieller Schaden für Unternehmen
    - Warum funktioniert das (hoffentlich) trotzdem?
        - durch hohe Kosten bei defekt machen Produzent·innen:
        - mehr Reperaturen
        - haltbarere Geräte
- Nebenwirkungen?
    - höhere Kosten
    - geringere Versorgung (Zugänglichkeit)
