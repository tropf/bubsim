# Starthypothese
- Einführungen abgeschlossen
- ran an die Simulation

1. Szenario Turing
    1. Durchführung
    2. kurze Auwertung
    3. Ziele der Akteure ansprechen (siehe Unten)
2. Szenario Zuse
    1. Durchführung
    2. Sammlung Beobachtungen
    3. Prüfung Hypothese
        - **nicht spoilern**
        - Feststellung: Garantie verlängern hilft nicht
        - nochmal: das sollen die Teilnehmer·innen von selbst entdecken
    4. Ist das auf die Realität übertragbar?
        - weitere Aspekte sammeln
    5. Wie könnten wir die Simulation Ändern, dass weniger Geräte neu gekauft werden.
        - als Parameter Moorsches Wachstum erklären
        - Vorschlagen: nur einen Parameter verändern
