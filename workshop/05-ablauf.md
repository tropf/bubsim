# Ablauf
- Teilnehmer·innen:
    - Nehmen die Rolle von Produzenten ein.
- mehrfach
- jeweils mit anderen Parametern (Garantiedauer, Kosten für Entwicklung...)

## Im Allgemeinen
1. Hypothese Formulieren
    - in Form von Parametern
    - mit erwarteten (erwünschten Ergebnissen)
    - **Die darf auch naiv sein**
        - das soll sie sogar, denn:
    - je einfacher die Hypothese
        - umso einfacher die Umsetzung
        - umso einfacher die Beobachtung
        - umso deutlicher das Ergebnis
        - (und wirkliche Komplexität können wir hier nicht leisten)
2. Simulation durchführen
    - Jede·r Teilnehmer·in für sich
3. Auswerten
    - Sammlung von Beobachtungen
    - Wird durch ein paar Diagramme gestützt
    - Hypothese vom Anfang bestätigen/verwerfen
    - Frage beantworten: Ist das auf die Realität übertragbar?
    - Ableiten von neuen Hypothesen
    - **Deshalb ist das ein Workshop:**
        - ohne aktives Beobachten und Einbringen von eigenen Ideen wird das nix
4. von Vorn

## Im Speziellen
- Wir haben schon eine Hypothese:
    - aus den Forderungen
- Mehr Garantiedauer = weniger neu gekaufte Geräte
    - da alte Geräte länger genutzt werden können
- daher die ersten zwei Runden:
    1. Standardszenario
    2. Lange Garantie
