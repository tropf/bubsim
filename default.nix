{ pkgs ? import <nixpkgs> {},
  stdenv ? pkgs.stdenv,
  runtimecfgPath ? "/etc/bubsim.cfg"
  }:
let
    pistache = stdenv.mkDerivation rec {
        pname = "pistache";
        version = "0b6cb29915e2b0fa915f53051efc5583b5ef74e6";

        src = pkgs.fetchurl {
            url = "https://github.com/pistacheio/${pname}/archive/${version}.tar.gz";
            sha256 = "0wzih8dyaxfbkwn3mfia9yji6xnrl4pks8hh5ij7rczp97336y5a";
        };

        buildInputs = with pkgs; [
            cmake
            stdenv.cc
            rapidjson
            pkg-config
            git
        ];
    };
in
stdenv.mkDerivation rec {
  name = "bubsim";
  version = "0.0.1";

  src = ./.;

  buildInputs = with pkgs; [
    # build stuff
    cmake
    cmakeCurses
    stdenv.cc
    git
    sassc

    # dependencies
    pistache
  ];

  cmakeFlags = [
      "-DBUBSIM_RUNTIME_CFG_PATH=${runtimecfgPath}"
  ];
}
