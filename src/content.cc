/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_CONTENT_INCLUDED__
#define __BUBSIM_CONTENT_INCLUDED__

#include <bubsim/content.h>

#include <string>

#include <bubsim/config.h>

#define INCBIN_PREFIX
#include <incbin.h>

using namespace std;

INCBIN(indexTemplate, BUBSIM_WWW_DIR "/index.html");
INCBIN(style, BUBSIM_BUILD_WWW_DIR "/style.css");
INCBIN(iconsCss, BUBSIM_WWW_DIR "/ionicons.css");
INCBIN(iconsEot, BUBSIM_WWW_DIR "/ionicons.eot");
INCBIN(iconsWoff, BUBSIM_WWW_DIR "/ionicons.woff");
INCBIN(iconsWoff2, BUBSIM_WWW_DIR "/ionicons.woff2");
INCBIN(iconsTtf, BUBSIM_WWW_DIR "/ionicons.ttf");
INCBIN(favicon, BUBSIM_WWW_DIR "/favicon.ico");
INCBIN(badgeBinaermensch, BUBSIM_WWW_DIR "/badges/binaermensch.svg");
INCBIN(badgeLaptopberg, BUBSIM_WWW_DIR "/badges/laptopberg.svg");
INCBIN(badgeLaptopkopf, BUBSIM_WWW_DIR "/badges/laptopkopf.svg");
INCBIN(badgeSchroete, BUBSIM_WWW_DIR "/badges/schroete.svg");

const string bubsim::content::indexTemplate((char *) indexTemplateData, indexTemplateSize);
const string bubsim::content::style((char *) styleData, styleSize);
const string bubsim::content::iconsCss((char *) iconsCssData, iconsCssSize);
const string bubsim::content::iconsEot((char *) iconsEotData, iconsEotSize);
const string bubsim::content::iconsWoff((char *) iconsWoffData, iconsWoffSize);
const string bubsim::content::iconsWoff2((char *) iconsWoff2Data, iconsWoff2Size);
const string bubsim::content::iconsTtf((char *) iconsTtfData, iconsTtfSize);
const string bubsim::content::favicon((char *) faviconData, faviconSize);
const string bubsim::content::badgeBinaermensch((char *) badgeBinaermenschData, badgeBinaermenschSize);
const string bubsim::content::badgeLaptopberg((char *) badgeLaptopbergData, badgeLaptopbergSize);
const string bubsim::content::badgeLaptopkopf((char *) badgeLaptopkopfData, badgeLaptopkopfSize);
const string bubsim::content::badgeSchroete((char *) badgeSchroeteData, badgeSchroeteSize);

#endif // __BUBSIM_CONTENT_INCLUDED__
