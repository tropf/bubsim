/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/scenario.h>

#include <functional>
#include <vector>

using namespace bubsim;
using std::vector;

bool bubsim::operator<(const scenario& lhs, const scenario& rhs) {
    if (lhs.name != rhs.name) {
        return lhs.name < rhs.name;
    } else if (lhs.description != rhs.description) {
        return lhs.description < rhs.description;
    } else if (lhs.params != rhs.params) {
        auto paramhasher = std::hash<simulationParameters>{};
        return paramhasher(lhs.params) < paramhasher(rhs.params);
    } else if (lhs.objectives != rhs.objectives) {
        auto ohasher = std::hash<vector<objective>>{};
        return ohasher(lhs.objectives) < ohasher(rhs.objectives);
    } else if (lhs.tutorial != rhs.tutorial) {
        return lhs.tutorial < rhs.tutorial;
    }

    // all equal
    return false;
}

bool bubsim::operator==(const scenario& lhs, const scenario& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}

bool bubsim::operator!=(const scenario& lhs, const scenario& rhs) {
    return !(lhs == rhs);
}

const vector<objective> bubsim::default_objectives = {
    objective_busy,
    objective_usage,
    objective_supply,
};

const vector<scenario> bubsim::default_scenarios = {
    {
        "Tutorial",
        "Laptophersteller·in zu sein ist kompliziert – und die Bedienung dieser Simulation auch: Ein Szenario zum kennenlernen.",
        {2, 36, 100, 10000, 75000, 0.7f},
        {
            objective_tutorial,
        },
        true
    },
    {
        "Turing",
        "An die Realität angelehntes Szenario mit schnellem technischem Fortschritt und kurzer Garantiedauer.",
        {1, 36, 100, 10000, 50000, 0.7f},
        default_objectives,
        false
    },
    {
        "Dijkstra",
        "Die Garantiedauer ist enorm hoch.",
        {10, 36, 100, 10000, 50000, 0.7f},
        default_objectives,
        false
    },
    {
        "Lovelace",
        "Szenario mit hoher Garantiedauer, langsamen technischer Fortschritt und wenig Hardwareanspruch der Kund·innen.",
        {10, 72, 100, 10000, 50000, 0.45f},
        default_objectives,
        false
    },
};
