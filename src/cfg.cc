/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/cfg.h>

#include <string>
#include <map>
#include <fstream>
#include <regex>
#include <stdexcept>

#include <bubsim/cfg.h>

using namespace bubsim;
using std::string;
using std::regex;
using std::regex_replace;

void bubsim::runtimeCfg::loadFromFile(string path) {
    std::ifstream file(path);

    if (!file.is_open()) {
        return;
    }

    string line;

    std::regex line_regex("^\\s*(\\S+)\\s+(\\S.*)?$");
    std::smatch m;

    while (std::getline(file, line)) {
        if (regex_match(line, m, line_regex)) {
            raw[m[1]] =
                regex_replace(
                              regex_replace(
                                            string(m[2]),
                                            regex("\\\\\\\\"),
                                            "\\"
                                            ),
                              regex("\\\\n"),
                              "\n"
                              );
        }
    }

    loadAttributesFromRaw();
}

static void setFromKeyMaybe(const std::map<string, string>& m, const string& key, string& value_ptr) {
    if (m.find(key) != m.end()) {
        value_ptr = m.at(key);
    }
}

static void setFromKeyMaybe(const std::map<string, string>& m, const string& key, int& value_ptr) {
    if (m.find(key) != m.end()) {
        try {
            value_ptr = std::stoi(m.at(key));
        } catch (std::invalid_argument &e) {
            // nop
            return;
        }
    }
}

void bubsim::runtimeCfg::loadAttributesFromRaw() {
    setFromKeyMaybe(raw, "imprint", imprint);
    setFromKeyMaybe(raw, "port", port);
    setFromKeyMaybe(raw, "displayredeemcodessecret", displayredeemcodessecret);
    setFromKeyMaybe(raw, "allcodesfile", allcodesfile);
    setFromKeyMaybe(raw, "redeemurl", redeemurl);
}
