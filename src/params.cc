/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/params.h>

#include <functional>
#include <cmath>

#include <bubsim/config.h>

using std::hash;

int bubsim::simulationParameters::currentGflops(int year) const {
    return 100 * pow(2, (year - BUBSIM_START_YEAR + 1) * (12.0f / mooresMonths));
}

bool bubsim::operator<(const simulationParameters& lhs, const simulationParameters& rhs) {
    auto hasher = hash<simulationParameters>{};
    return hasher(lhs) < hasher(rhs);
}

bool bubsim::operator==(const simulationParameters& lhs, const simulationParameters& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}

bool bubsim::operator!=(const simulationParameters& lhs, const simulationParameters& rhs) {
    return !(lhs == rhs);
}
