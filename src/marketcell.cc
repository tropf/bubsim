/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/marketcell.h>

#include <random>
#include <map>

#include <bubsim/model.h>
#include <bubsim/params.h>

using namespace bubsim;
using namespace std;

bubsim::marketCell::marketCell(struct simulationParameters givenParams) {
    params = givenParams;

    currentModel = emptyModel;
    
    random_device dev;
    mt19937 rng(dev());
    normal_distribution<> ageDist{4, 1}, priceDist{(double) params.avgCustomerAcceptedPrice, 40}, performanceDist{params.acceptedPerformanceFractionAvg, 0.1};

    maxAge = ageDist(rng);
    maxPrice = priceDist(rng);
    minPerformance = performanceDist(rng);
}

int bubsim::marketCell::expectedPerformance(int year) {
    return minPerformance * params.currentGflops(year);
}

bubsim::throwingOutReason bubsim::marketCell::advanceYear(int targetYear) {
    if (emptyModel == currentModel) {
        return bubsim::throwingOutReason::hadNone;
    }

    random_device dev;
    mt19937 rng(dev());
    uniform_real_distribution<> dist(0.0, 1.0);

    // throw out broken device
    if (dist(rng) > currentModel.durability) {
        currentModel = emptyModel;
        return bubsim::throwingOutReason::broken;
    }

    if (currentModel.gflops < expectedPerformance(targetYear)) {
        currentModel = emptyModel;
        return bubsim::throwingOutReason::performance;
    }

    return bubsim::throwingOutReason::keep;
}

model bubsim::marketCell::pickModel(int currentYear, map<model, int> priceByModel) {
    vector<model> candidates;
    
    for (const auto& it : priceByModel) {
        // check price
        if (priceByModel[it.first] > maxPrice) {
            continue;
        }

        // check gflops (performance)
        if (it.first.gflops < expectedPerformance(currentYear)) {
            continue;
        }

        candidates.push_back(it.first);
    }


    if (0 == candidates.size()) {
        return emptyModel;
    }

    random_device dev;
    mt19937 rng(dev());
    uniform_int_distribution<> dist(0, candidates.size() - 1);

    model pick = candidates[dist(rng)];

    boughtYear = currentYear;
    boughtPrice = priceByModel[pick];
    currentModel = pick;

    return pick;
}
