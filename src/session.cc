/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/session.h>

#include <map>
#include <list>
#include <string>

#include <bubsim/config.h>
#include <bubsim/scenario.h>
#include <bubsim/state.h>
#include <bubsim/util.h>

using std::string;

int bubsim::annual_accounts::total_revenue() const {
    return sales;
}

int bubsim::annual_accounts::total_expenses() const {
    return development + production + warranty;
}

bool bubsim::session_state::should_restore_scroll() {
    if(restore_scroll_next_load) {
        restore_scroll_next_load = false;
        return true;
    }
    
    return false;
}

void bubsim::session_state::maybeStoreForComparison(std::map<scenario, std::list<globalStats>>& storage) {
    if (stored_for_comparison) {
        // already stored
        return;
    }
    if (isRunning()) {
        // not done
        return;
    }

    storage[current_scenario].push_front(current_state.stats);
    stored_for_comparison = true;

    while (storage[current_scenario].size() > BUBSIM_MAX_COMPARISON_STATS) {
        storage[current_scenario].pop_back();
    }
}

bool bubsim::session_state::isRunning() {
    return current_state.year < (BUBSIM_START_YEAR + BUBSIM_YEARS_PER_ROUND);
}

void bubsim::session_state::getObjectiveRedeemCodes(const map<string, string>& cfg) {
    if (isRunning()) {
        // objectives only count after the game is done
        return;
    }

    for (const auto& o : current_scenario.objectives) {
        if (o.progress(current_state) >= 1.0f) {
            string key = o.rc3id + "_token_permanent";
            if (cfg.find(key) != cfg.end()) {
                redeem_codes[o] = getStrippedString(cfg.at(key));
            } else {
                redeem_codes[o] = "FEHLER";
            }
        }
    }
}
