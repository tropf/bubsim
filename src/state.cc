/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/state.h>

#include <algorithm>
#include <map>
#include <vector>
#include <random>
#include <cmath>

#include <bubsim/config.h>
#include <bubsim/model.h>
#include <bubsim/marketcell.h>
#include <bubsim/objective.h>

using namespace bubsim;
using namespace std;

bubsim::state::state(simulationParameters givenParams) {
    params = givenParams;

    // test data
    money = params.startingMoney;
    year = BUBSIM_START_YEAR;

    for (int i = 0; i < BUBSIM_MARKET_CELL_COUNT; i++) {
        // generate new cell each time instead of copying it
        marketCell c(params);
        market.push_back(c);
    }
}

bool bubsim::state::canProduce(const model& m) {
    int total_cost = BUBSIM_BATCH_SIZE * m.cost;
    
    if (find(blueprints.begin(), blueprints.end(), m) == blueprints.end()) {
        return false;
    }

    if (total_cost > money) {
        return false;
    }

    return true;
}

state bubsim::state::produce(const model& m) {
    state s = *this;
    if (!canProduce(m)) {
        return s;
    }

    int total_cost = BUBSIM_BATCH_SIZE * m.cost;
    s.stock[m] += BUBSIM_BATCH_SIZE;
    s.money -= total_cost;
    s.stats.productionCost += total_cost;
    s.stats.productionCnt += BUBSIM_BATCH_SIZE;

    return s;
}

bool bubsim::state::canDevelopBlueprint() {
    return money >= params.developmentCost;
}

state bubsim::state::developBlueprint(float focus_durability, float focus_gflops) {
    state s = *this;
    
    if (!canDevelopBlueprint()) {
        return s;
    }

    s.money -= params.developmentCost;
    s.stats.blueprintCost += params.developmentCost;
    s.stats.blueprintCnt++;

    random_device dev;
    mt19937 rng(dev());

    normal_distribution<> cost_dist{0.0, 5};
    float cost_base = 80;
    float cost_durability = 12 * focus_durability;
    float cost_performance = 12 * focus_gflops;
    float cost_rdm_part = cost_dist(rng);
    float cost_total = cost_base + cost_durability + cost_performance + cost_rdm_part;
    if (75 > cost_total) {
        cost_total = 75;
    }

    normal_distribution<> durability_dist{0.0, 0.03};
    float durability_base = 0.85;
    float durability_bonus = 0.1 * (4 * pow(focus_durability - 0.5, 3) + 0.5);
    float durability_rdm_part = durability_dist(rng);
    float durability_total = durability_base + durability_bonus + durability_rdm_part;

    durability_total = round(durability_total * 100) / 100.0;
    if (0.99 < durability_total) {
        durability_total = 0.99;
    }

    normal_distribution<> gflops_dist{1.0, 0.05};
    float gflops_base = params.currentGflops(year);
    float gflops_bonus = 0.75 + focus_gflops * 0.5;
    float gflops_rdm_part = gflops_dist(rng);
    float gflops_total = gflops_base * gflops_bonus * gflops_rdm_part;

    model m;
    m.cost = cost_total;
    m.durability = durability_total;
    m.gflops = gflops_total;
    m.name = getNewModelName();
    m.year = s.year;

    s.blueprints.push_back(m);
    s.stock[m] = 0;
    s.pricing[m] = 2 * m.cost;

    return s;
}

int bubsim::state::getYear() {
    return year;
}

int bubsim::state::getMoney() {
    return money;
}

vector<model> bubsim::state::getBlueprints() {
    return blueprints;
}

map<model, int> bubsim::state::getStock() {
    return stock;
}

map<model, int> bubsim::state::getPricing() {
    return pricing;
}

bool bubsim::state::modelExists(const string& name) {
    for (const auto& m : blueprints) {
        if (m.name == name) {
            return true;
        }
    }

    return false;
}

string bubsim::state::getNewModelName() {
    vector<string> names = {"Lumpi", "Leo", "Horst", "Jochen", "Hilde", "Lina", "Ute", "Traudel"};

    random_device dev;
    mt19937 rng(dev());
    uniform_int_distribution<mt19937::result_type> dist(0, names.size() - 1);
    
    string mainpart = names[dist(rng)];
    int cnt = 1;
    while(modelExists(mainpart + " " + to_string(cnt))) {
        cnt++;
    }

    return mainpart + " " + to_string(cnt);
}

model bubsim::state::getModelByName(const string& name) {
    for (const auto& m : blueprints) {
        if (m.name == name) {
            return m;
        }
    }

    return model();
}

void bubsim::state::setPrice(const model& m, int newprice) {
    if (!modelExists(m.name)) {
        return;
    }

    pricing[m] = newprice;
}

map<model, int> bubsim::state::getAvailableModelPrices() {
    map<model, int> available;

    for (const auto& it : stock) {
        if (it.second != 0) {
            available[it.first] = pricing[it.first];
        }
    }

    return available;
}

void bubsim::state::updateMaxs() {
    for (const auto& bp : blueprints) {
        if (bp.cost > maxs.cost) {
            maxs.cost = bp.cost;
        }
        if (bp.gflops > maxs.gflops) {
            maxs.gflops = bp.gflops;
        }
        if ((stock[bp] + sales[bp]) > maxs.existingCnt) {
            maxs.existingCnt = stock[bp] + sales[bp];
        }
    }
}

void bubsim::state::advanceYear() {
    warrantyCases.clear();
    int targetYear = year + 1;
    // total income this year
    int salesIncome = 0;
    int warrantyCosts = 0;
    // simulate each cell

    for (auto& cell : market) {
        model m_old = cell.currentModel;
        auto reason = cell.advanceYear(targetYear);
        model m_new = cell.currentModel;

        if (reason == bubsim::throwingOutReason::broken) {
            // model broke

            stats.brokenCnt++;
            stats.totalUsageYears += targetYear - cell.boughtYear;

            if (cell.boughtYear + params.warrantyYears >= year) {
                warrantyCosts += cell.boughtPrice;
                warrantyCases[m_old]++;
                stats.warrantyCnt++;
                stats.warrantyCost += cell.boughtPrice;
           }
        }

        if (reason == bubsim::throwingOutReason::performance) {
            stats.thrownOutPerformanceCnt++;
            stats.totalUsageYears += targetYear - cell.boughtYear;
        }

        model newModel = emptyModel;
        if (cell.currentModel == emptyModel) {
            newModel = cell.pickModel(year, getAvailableModelPrices());
        }

        // actual sale
        if (newModel != emptyModel) {
            // make sale
            sales[newModel]++;
            stock[newModel]--;
            salesIncome += pricing[newModel];
            stats.salesCnt++;
            stats.salesEarnings += pricing[newModel];
        }

        // check if supplied
        if (emptyModel == cell.currentModel) {
            stats.suppliedNotCnt++;
        } else {
            stats.suppliedCnt++;
        }
    }
    
    money += salesIncome;
    money -= warrantyCosts;
    year = targetYear;

    updateMaxs();
}

int bubsim::globalStats::totalThrownOut() const {
    return brokenCnt + thrownOutPerformanceCnt;
}

float bubsim::globalStats::avgUsage() const {
    if (0 == totalThrownOut()) {
        return 0;
    }

    return (float) totalUsageYears / (float) totalThrownOut();
}
