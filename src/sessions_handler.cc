/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/sessions_handler.h>

#include <random>
#include <limits>
#include <string>
#include <vector>
#include <chrono>

#include <bubsim/config.h>
#include <bubsim/scenario.h>

using namespace bubsim;
using namespace std::chrono;
using std::to_string;

string bubsim::SessionsHandler::generateSession(scenario scen) {
    std::random_device dev;
    std::mt19937 gen(dev());
    std::uniform_int_distribution<unsigned long long>
        dist(std::numeric_limits<unsigned long long>::min(),
             std::numeric_limits<unsigned long long>::max());

    session_state new_session(scen);
    new_session.session_id = to_string(dist(gen));
    while (sessionExists(new_session.session_id)) {
        new_session.session_id = to_string(dist(gen));
    }
    new_session.last_seen = steady_clock::now();

    sessions[new_session.session_id] = new_session;

    return new_session.session_id;
}

bool bubsim::SessionsHandler::sessionExists(std::string session_key) {
    return sessions.find(session_key) != sessions.end();
}

void bubsim::SessionsHandler::cleanupSessions() {
    std::vector<std::string> timeouted;

    for (const auto& it : sessions) {
        auto age = steady_clock::now() - it.second.last_seen;
        if (duration_cast<seconds>(age).count() > BUBSIM_SESSION_TIMEOUT_SECONDS) {
            timeouted.push_back(it.first);
        }
    }

    for (const auto& to_delete : timeouted) {
        sessions.erase(to_delete);
    }
}

session_state bubsim::SessionsHandler::getOrCreateSession(std::string session_key, scenario scen) {
    cleanupSessions();

    if (!sessionExists(session_key)) {
        session_state new_session(scen);
        new_session.session_id = session_key;
        sessions[session_key] = new_session;
    }
    sessions[session_key].last_seen = steady_clock::now();
    return sessions[session_key];
}
