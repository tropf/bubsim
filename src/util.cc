/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/util.h>

#include <algorithm>
#include <string>
#include <sstream>
#include <iomanip>
#include <regex>
#include <random>

using namespace std;

string bubsim::urlEncode(const string &value) {
    ostringstream escaped;
    escaped.fill('0');
    escaped << hex;

    for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
        string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << uppercase;
        escaped << '%' << setw(2) << int((unsigned char) c);
        escaped << nouppercase;
    }

    return escaped.str();
}

string bubsim::urlDecode(const string &SRC) {
    string ret;
    char ch;
    int i, ii;
    for (i=0; i<SRC.length(); i++) {
        if (int(SRC[i])==37) {
            sscanf(SRC.substr(i+1,2).c_str(), "%x", &ii);
            ch=static_cast<char>(ii);
            ret+=ch;
            i=i+2;
        } else {
            ret+=SRC[i];
        }
    }
    return (ret);
}


string bubsim::getSafeString(const string& unsafe) {
    return regex_replace(unsafe, regex("[^a-zA-Z0-9]"), "");
}

string bubsim::getRandomString(size_t length) {
    const string chars = "0123456789ABCDEFGHJKLMNPQRSTUVWXYZ";

    static std::random_device dev;
    static std::mt19937 gen(dev());
    static std::uniform_int_distribution<int> dist(0, chars.size() - 1);

    string rdmstring;
    while (rdmstring.size() < length) {
        rdmstring += chars[dist(gen)];
    }

    return rdmstring;
}

string bubsim::getStrippedString(const string& s) {
    return regex_replace(regex_replace(s, regex("^\\s*"), ""), regex("\\s+$"), "");
}
