/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/objective.h>

#include <functional>

#include <bubsim/config.h>
#include <bubsim/state.h>

using namespace bubsim;

bool bubsim::operator<(const objective& lhs, const objective& rhs) {
    auto hasher = std::hash<objective>{};
    return hasher(lhs) < hasher(rhs);
}

bool bubsim::operator==(const objective& lhs, const objective& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}

bool bubsim::operator!=(const objective& lhs, const objective& rhs) {
    return !(lhs == rhs);
}

float bubsim::progressReachYear(state s) {
    int current = s.year - BUBSIM_START_YEAR;
    int goal = BUBSIM_YEARS_PER_ROUND;
    if (current < 0) {
        return 0;
    }
    if (current > goal) {
        return 1;
    }

    return ((float) current) / ((float) goal);
}

float bubsim::progressReachTotalSales(state s, int goal) {
    int current = s.stats.salesEarnings;
    if (current < 0) {
        return 0;
    }
    if (current > goal) {
        return 1;
    }

    return ((float) current) / ((float) goal);
}

float bubsim::progressReachAvgUsage(state s, float goal) {
    float current = s.stats.avgUsage();
    if (current < 0) {
        return 0;
    }
    if (current > goal) {
        return 1;
    }

    return ((float) current) / ((float) goal);
}

float bubsim::progressReachSupply(state s, float goal_fraction) {
    int current = s.stats.suppliedCnt;
    int goal_absolute = goal_fraction * BUBSIM_MARKET_CELL_COUNT * BUBSIM_YEARS_PER_ROUND;
    if (current < 0) {
        return 0;
    }
    if (current > goal_absolute) {
        return 1;
    }

    return ((float) current) / ((float) goal_absolute);
}

