/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <bubsim/mstch_helper.h>

#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <map>
#include <regex>

#include <mustache.hpp>

#include <bubsim/config.h>
#include <bubsim/state.h>
#include <bubsim/model.h>
#include <bubsim/objective.h>
#include <bubsim/params.h>
#include <bubsim/scenario.h>
#include <bubsim/util.h>

using std::to_string;
using std::string;
using namespace kainjow;
using namespace bubsim;

mustache::data bubsim::toMstch(const globalStats& s) {
    mustache::data d;

    d.set("total_production_cnt", to_string(s.productionCnt));
    d.set("total_production_cost", to_string(s.productionCost));
    d.set("total_sales_earnings", to_string(s.salesEarnings));
    d.set("total_sales_cnt", to_string(s.salesCnt));
    d.set("total_warranty_cost", to_string(s.warrantyCost));
    d.set("total_warranty_cnt", to_string(s.warrantyCnt));
    d.set("total_bp_cost", to_string(s.blueprintCost));
    d.set("total_bp_cnt", to_string(s.blueprintCnt));
    d.set("total_thrown_out_performance_cnt", to_string(s.thrownOutPerformanceCnt));
    d.set("total_broken_cnt", to_string(s.brokenCnt));
    d.set("avg_usage_years", to_string(s.avgUsage()));

    // note: int, not round so there are no complaints about the achievement; because that triggers *above* 35%, but with round() 35% would be displayed from >=34.5%
    d.set("avg_supply_percentage", to_string((int) ((100.0f * (float) s.suppliedCnt / (s.suppliedCnt + s.suppliedNotCnt)))));
    d.set("total_broken_no_warranty", to_string(s.brokenCnt - s.warrantyCnt));

    float laptops_produced = s.productionCnt;
    float laptops_in_stock = laptops_produced - s.salesCnt;
    float laptops_sold = s.salesCnt;
    d.set("bar_laptops_sold", to_string(100 * (laptops_sold / laptops_produced )));
    d.set("bar_laptops_stock", to_string(100 * (laptops_in_stock / laptops_produced)));

    d.set("total_stock", to_string((int) laptops_in_stock));

    float thrownout_performance = s.thrownOutPerformanceCnt;
    float thrownout_broken = s.brokenCnt;
    float in_use = laptops_sold - thrownout_broken - thrownout_performance;

    d.set("bar_laptoplife_broken", to_string(100 * (thrownout_broken / laptops_sold)));
    d.set("bar_laptoplife_performance", to_string(100 * (thrownout_performance / laptops_sold)));
    d.set("bar_laptoplife_in_use", to_string(100 * (in_use / laptops_sold)));

    d.set("total_in_use", to_string((int) in_use));

    float broken_warranty = s.warrantyCnt;
    float broken_other = thrownout_broken - broken_warranty;

    d.set("bar_broken_warranty", to_string(100 * (broken_warranty / thrownout_broken)));
    d.set("bar_broken_other", to_string(100 * (broken_other / thrownout_broken)));

    float cost_blueprints = s.blueprintCost;
    float cost_production = s.productionCost;
    float cost_warranty = s.warrantyCost;
    float cost_total = cost_blueprints + cost_production + cost_warranty;
    float money_bar_max = std::max<float>(cost_total, s.salesEarnings);

    d.set("net_total", to_string((int) (s.salesEarnings - cost_total)));

    d.set("bar_income_sales", to_string(100 * (((float) s.salesEarnings) / money_bar_max)));
    d.set("bar_spending_blueprints", to_string(100 * (cost_blueprints / money_bar_max)));
    d.set("bar_spending_production", to_string(100 * (cost_production / money_bar_max)));
    d.set("bar_spending_warranty", to_string(100 * (cost_warranty / money_bar_max)));

    d.set("total_spending", to_string((int) cost_total));

    return d;
}

mustache::data bubsim::toMstch(const model& m) {
    mustache::data d;

    d.set("cost", to_string(m.cost));
    d.set("year", to_string(m.year));
    d.set("name", m.name);
    d.set("gflops", to_string(m.gflops));
    d.set("durability_percent", to_string(int (round(m.durability * 100))));

    d.set("name_url", urlEncode(m.name));

    return d;
}

mustache::data bubsim::toMstch(const objective& o, const state& s, const std::map<objective, string>& redeem_codes, const string& redeemurl_pattern) {
    mustache::data d;

    d.set("name", o.name);
    d.set("description", o.description);
    if ("" != o.icon) {
        d.set("icon", o.icon);
    }
    d.set("progress_percent", to_string(int (round(o.progress(s) * 100))));
    d.set("achieved", (s.year >= BUBSIM_START_YEAR + BUBSIM_YEARS_PER_ROUND) && (1 <= o.progress(s)));

    if (redeem_codes.find(o) != redeem_codes.end()) {
        d.set("redeem_code", redeem_codes.at(o));
        if (!redeemurl_pattern.empty()) {
            d.set("redeem_url", std::regex_replace(redeemurl_pattern, std::regex("TOKEN"), urlEncode(redeem_codes.at(o))));
        }
    }

    return d;
}

mustache::data bubsim::toMstch(const vector<objective>& o_list, const state& s, const std::map<objective, string>& redeem_codes, const std::string& redeemurl_pattern) {
    mustache::data l{mustache::data::type::list};

    for (const auto o : o_list) {
        l << toMstch(o, s, redeem_codes, redeemurl_pattern);
    }

    return l;
}

mustache::data bubsim::toMstch(const simulationParameters& p) {
    mustache::data d;

    d.set("warranty_years", to_string(p.warrantyYears));
    d.set("moores_months", to_string(p.mooresMonths));
    d.set("customer_avg_price", to_string(p.avgCustomerAcceptedPrice));
    d.set("bp_cost", to_string(p.developmentCost));
    d.set("starting_money", to_string(p.startingMoney));
    d.set("accepted_performance_avg", to_string(p.acceptedPerformanceFractionAvg));
    d.set("accepted_performance_avg_percent", to_string((int) (p.acceptedPerformanceFractionAvg * 100)));

    return d;
}

mustache::data bubsim::toMstch(const scenario& s) {
    mustache::data d;

    d.set("name", s.name);
    d.set("description", s.description);
    d.set("params", toMstch(s.params));
    d.set("objectives", toMstch(s.objectives, state(s.params)));

    return d;
}

mustache::data bubsim::toMstch(const annual_accounts& acc) {
    mustache::data d;

    int bar_max = std::max<int>(acc.total_expenses(), acc.total_revenue());

    d.set("year", to_string(acc.year));

    d.set("sales", to_string(acc.sales));
    d.set("sales_bar", to_string(100.0f * ((float) acc.sales / (float) bar_max)));

    d.set("development", to_string(acc.development));
    d.set("development_bar", to_string(100.0f * ((float) acc.development / (float) bar_max)));

    d.set("production", to_string(acc.production));
    d.set("production_bar", to_string(100.0f * ((float) acc.production / (float) bar_max)));

    d.set("warranty", to_string(acc.warranty));
    d.set("warranty_bar", to_string(100.0f * ((float) acc.warranty / (float) bar_max)));

    return d;
}
