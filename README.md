# bubsim
small project for a workshop to demonstrate the causes behind the lifetime of laptops (or any digital devices for that matter)

currently entirely in german (i18n is not planned for now)

The design principle is "quick and dirty", this thing is only touched when needed.
So I am (and we are) sorry for the quality of code. This more prototype than anything else.

## Building and Running
```
cd bubsim
git submodule update --init --recursive
mkdir build && cd build
cmake ..
make -j $(( 2 * $(nproc) ))
```

Then run `./bubsim`, which will launch a web server listening on `0.0.0.0:9080`.
