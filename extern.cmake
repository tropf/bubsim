include(ExternalProject)

# Pistache
###

#set(PISTACHE_INSTALL_DIR "${PROJECT_BINARY_DIR}/extern/pistache")
#ExternalProject_Add(pistache_extern
#    PREFIX "${PROJECT_BINARY_DIR}/extern/pistache"
#    SOURCE_DIR "${PROJECT_SOURCE_DIR}/extern/pistache"
#    CMAKE_CACHE_ARGS "-DCMAKE_INSTALL_PREFIX:STRING=${PISTACHE_INSTALL_DIR}"
#)
#
## to make cmake shut up about non-existent directories during configuration
#file(MAKE_DIRECTORY "${PISTACHE_INSTALL_DIR}/include")
#
#add_library(pistache STATIC IMPORTED)
#set_target_properties(pistache PROPERTIES
#    IMPORTED_LOCATION "${PISTACHE_INSTALL_DIR}/lib/libpistache.a"
#    INTERFACE_INCLUDE_DIRECTORIES "${PISTACHE_INSTALL_DIR}/include"
#    )
#add_dependencies(pistache pistache_extern)

# Mustache
###

add_library(mustache INTERFACE IMPORTED)
set_target_properties(mustache PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${PROJECT_SOURCE_DIR}/extern/Mustache"
    )

# Spectre
###

find_program(SASSC_EXE sassc)
if (NOT SASSC_EXE)
    message(FATAL_ERROR "sassc not found (required for building)")
endif()

set(SPECTRE_DIR "${PROJECT_SOURCE_DIR}/extern/spectre")
# create dir, so built css can be placed
file(MAKE_DIRECTORY "${PROJECT_BINARY_DIR}/www")

add_custom_target(spectre)

add_custom_command(
    TARGET spectre
    PRE_BUILD
    COMMAND ${SASSC_EXE} "--style" "compressed" "--load-path" "${SPECTRE_DIR}/src" "${PROJECT_SOURCE_DIR}/www/style.scss" "${PROJECT_BINARY_DIR}/www/style.css"
    )

# Incbin
###

add_library(incbin INTERFACE IMPORTED)
set_target_properties(incbin PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${PROJECT_SOURCE_DIR}/extern/incbin"
    )
