/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <iostream>
#include <string>
#include <sstream>
#include <mutex>
#include <csignal>
#include <map>
#include <utility>
#include <random>
#include <limits>
#include <chrono>
#include <algorithm>
#include <regex>
#include <list>
#include <functional>
#include <set>
#include <fstream>

#include <bubsim/bubsim.h>

#include <pistache/endpoint.h>
#include <pistache/http_headers.h>

#include <mustache.hpp>

using namespace std;
using namespace Pistache;
using namespace Pistache::Http;
using namespace bubsim;
using namespace std::chrono;

runtimeCfg cfg;

struct BubSimHandler : public Http::Handler, public bubsim::SessionsHandler {
private:
    map<scenario, list<globalStats>> stats_for_comparison;
    map<objective, set<string>> all_redeemcodes;

public:
    HTTP_PROTOTYPE(BubSimHandler)

    void redirectSession(const Http::Request& req, Http::ResponseWriter& response, string session_id = "") {
        if ("" == session_id) {
            session_id = getSessionId(req);
        }

        response.headers().add(make_shared<Header::Location>("/" + session_id + "/main"));
        response.send(Code::See_Other);
    }

    /**
     * loads existing session or creates new one
     */
    session_state makeSessionHappen(const Http::Request& req, Http::ResponseWriter& response) {
        cleanupSessions();
        string current_session_id = getSessionId(req);
        return getOrCreateSession(current_session_id, default_scenarios[0]);
    }

    void index(session_state& ss, const Http::Request& req, Http::ResponseWriter& response) {
        kainjow::mustache::mustache tmpl(bubsim::content::indexTemplate);
        kainjow::mustache::data d;
        bubsim::state s = ss.current_state;
        bool running = s.getYear() - BUBSIM_START_YEAR < BUBSIM_YEARS_PER_ROUND;

        d.set("session_id", ss.session_id);
        d.set("version", BUBSIM_VERSION);
        d.set("year", to_string(s.getYear()));
        d.set("last_year", to_string(s.getYear() - 1));
        d.set("is_final_year", s.getYear() == (BUBSIM_START_YEAR + BUBSIM_YEARS_PER_ROUND - 1));
        d.set("money", to_string(s.getMoney()));
        d.set("batch_size", to_string(BUBSIM_BATCH_SIZE));
        d.set("bp_cost", to_string(s.params.developmentCost));
        d.set("bp_can_develop", s.canDevelopBlueprint());
        if (!running) {
            d.set("final_stats", toMstch(s.stats));
        }
        d.set("running", running);
        d.set("show_objectives", true);
        d.set("new_session", false);
        d.set("year_progress", to_string(100 * ((float) (s.getYear() - BUBSIM_START_YEAR) / (float) (BUBSIM_YEARS_PER_ROUND - 1))));
        d.set("ending_year", to_string(BUBSIM_START_YEAR + BUBSIM_YEARS_PER_ROUND - 1));
        d.set("years_per_round", to_string(BUBSIM_YEARS_PER_ROUND));
        d.set("is_tutorial", ss.current_scenario.tutorial);
        if (!cfg.imprint.empty()) {
            d.set("imprint", cfg.imprint);
        }

        d.set("restore_scroll", ss.restore_scroll_next_load);
        // ss is not written back, restore in actual session storage
        sessions[ss.session_id].restore_scroll_next_load = false;

        if (s.year > BUBSIM_START_YEAR) {
            d.set("accounts", toMstch(ss.accounts));
        }
        d.set("params", toMstch(s.params));
        d.set("objectives", toMstch(ss.current_scenario.objectives, ss.current_state, ss.redeem_codes, cfg.redeemurl));

        s.updateMaxs();
        d.set("max_cost", to_string(s.maxs.cost));
        d.set("max_gflops", to_string(s.maxs.gflops));
        d.set("max_existing", to_string(s.maxs.existingCnt));

        kainjow::mustache::data years_list{kainjow::mustache::data::type::list};
        for (int i = 0; i < BUBSIM_YEARS_PER_ROUND; i++) {
            kainjow::mustache::data list_entry;
            list_entry.set("year", to_string(i + BUBSIM_START_YEAR));
            list_entry.set("current", i + BUBSIM_START_YEAR == s.getYear());
            list_entry.set("first", 0 == i);
            list_entry.set("last", BUBSIM_YEARS_PER_ROUND - 1 == i);
            years_list << list_entry;
        }
        d.set("years_list", years_list);

        kainjow::mustache::data current_comparison_stats{kainjow::mustache::data::type::list};
        for (const auto& s : stats_for_comparison[ss.current_scenario]) {
            current_comparison_stats << toMstch(s);
        }
        d.set("comparison_stats", current_comparison_stats);

        kainjow::mustache::data sales{kainjow::mustache::data::type::list};
        for (const auto& it : s.sales) {
            kainjow::mustache::data sale;
            sale.set("model_name", it.first.name);
            sale.set("price_unit", to_string(s.pricing[it.first]));
            sale.set("unit_cnt", to_string(s.sales[it.first]));
            sale.set("total_revenue", to_string(s.pricing[it.first] * s.sales[it.first]));
            sales << sale;
        }
        d.set("sales", sales);

        kainjow::mustache::data models{kainjow::mustache::data::type::list};
        auto bps = s.getBlueprints();
        int total_stock = 0;
        for (vector<model>::reverse_iterator rit = bps.rbegin(); rit != bps.rend(); ++rit) {
            const model bp = *rit;
            auto model = toMstch(bp);
            model.set("stock", to_string(s.getStock()[bp]));
            model.set("is_stocked", 0 < s.getStock()[bp]);
            model.set("price", to_string(s.getPricing()[bp]));
            model.set("name_safe", getSafeString(bp.name));
            if (s.sales.find(bp) != s.sales.end()) {
                model.set("sold", to_string(s.sales[bp]));
            } else {
                model.set("sold", "0");
            }

            model.set("batch_cost", to_string(BUBSIM_BATCH_SIZE * bp.cost));
            model.set("can_produce", BUBSIM_BATCH_SIZE * bp.cost <= s.getMoney());

            model.set("bar_stacked_stock", to_string(0 < s.maxs.existingCnt ? 100.0f * ((float) s.getStock()[bp] / s.maxs.existingCnt) : 0));
            model.set("bar_stacked_sold", to_string(0 < s.maxs.existingCnt ? 100.0f * ((float) s.sales[bp] / s.maxs.existingCnt): 0));
            model.set("bar_cost", to_string(0 < s.maxs.cost ? 100.0f * ((float) bp.cost / s.maxs.cost) : 0));
            model.set("bar_gflops", to_string(0 < s.maxs.gflops ? 100.0f * ((float) bp.gflops / s.maxs.gflops) : 0));
            
            models << model;

            total_stock += s.getStock()[bp];
        }
        d.set("models", models);

        d.set("is_advancable", !(s.year == BUBSIM_START_YEAR && !total_stock));

        string rendered = tmpl.render(d);

        response.send(Code::Ok, rendered, MIME(Text, Html));
    }

    void generateObjectiveRedeemcodes(session_state& s, const Http::Request& req, Http::ResponseWriter& resp) {
        s.getObjectiveRedeemCodes(cfg.raw);
        ofstream codesfile;
        if (!cfg.allcodesfile.empty()) {
            codesfile.open(cfg.allcodesfile, ios::app | ios::out);
        }

        for (const auto& it : s.redeem_codes) {
            auto insert_result = all_redeemcodes[it.first].insert(it.second);
            if (insert_result.second && codesfile.is_open()) {
                // inserted new code into set
                codesfile << it.first.rc3id << ":" << it.second << "\n";
            }
        }

        if (codesfile.is_open()) {
            codesfile.close();
        }

        redirectSession(req, resp, s.session_id);
    }

    void developBlueprint(session_state& s, const Http::Request& req, Http::ResponseWriter& response) {
        float focus_durability = 0;
        float focus_performance = 0;

        focus_durability = (stoi(req.query().get("durability").getOrElse("100")) - 100)/100.0;
        focus_performance = (stoi(req.query().get("performance").getOrElse("100")) - 100)/100.0;

        s.current_state = s.current_state.developBlueprint(focus_durability, focus_performance);
    }

    void produceBatch(session_state& s, const Http::Request& req, Http::ResponseWriter& response) {
        string model_name = urlDecode(req.query().get("producebatch").getOrElse(""));

        for (const auto& bp : s.current_state.getBlueprints()) {
            if (getSafeString(bp.name) == model_name) {
                s.current_state = s.current_state.produce(s.current_state.getModelByName(bp.name));
                // produced batch: scroll to btn
                s.restore_scroll_next_load = true;

                return;
            }
        }
    }

    void updatePrices(session_state& s, const Http::Request& req, Http::ResponseWriter& response) {
        string updated_bp_name = "";

        for (const auto& bp : s.current_state.getBlueprints()) {
            string field_name = getSafeString(bp.name) + "_price";

            if (req.query().has(field_name)) {
                s.current_state.setPrice(bp, stoi(req.query().get(field_name).get()));
            }
        }
    }

    void advanceYear(session_state& s, const Http::Request& req, Http::ResponseWriter& response) {
        s.current_state.advanceYear();

        globalStats new_stats = s.current_state.stats;
        s.accounts.sales = new_stats.salesEarnings - s.old_stats.salesEarnings;
        s.accounts.development = new_stats.blueprintCost - s.old_stats.blueprintCost;
        s.accounts.production = new_stats.productionCost - s.old_stats.productionCost;
        s.accounts.warranty = new_stats.warrantyCost - s.old_stats.warrantyCost;
        s.accounts.year = s.current_state.year - 1;

        s.old_stats = new_stats;

        s.maybeStoreForComparison(stats_for_comparison);
    }

    void newCustomSession(const Http::Request& req, Http::ResponseWriter& response) {
        simulationParameters p;

        p.warrantyYears = stoi(req.query().get("param_warranty_years").getOrElse(to_string(defaultParameters.warrantyYears)));
        p.mooresMonths = stoi(req.query().get("param_moores_months").getOrElse(to_string(defaultParameters.mooresMonths)));
        p.avgCustomerAcceptedPrice = stoi(req.query().get("param_customer_avg_price").getOrElse(to_string(defaultParameters.avgCustomerAcceptedPrice)));
        p.developmentCost = stoi(req.query().get("param_bp_cost").getOrElse(to_string(defaultParameters.developmentCost)));
        p.startingMoney = stoi(req.query().get("param_starting_money").getOrElse(to_string(defaultParameters.startingMoney)));
        p.acceptedPerformanceFractionAvg = stof(req.query().get("param_accepted_performance_avg").getOrElse(to_string(defaultParameters.acceptedPerformanceFractionAvg)));

        scenario scen;
        scen.name = "Benutzerdefiniertes Szenario";
        scen.description = "Ein angepasstes Szenario ohne Achievements";
        scen.params = p;
        scen.tutorial = false;
        scen.objectives = {};

        redirectSession(req, response, generateSession(scen));
    }

    void newSession(const Http::Request& req, Http::ResponseWriter& response) {
        int scenario_index = stoi(req.query().get("index").getOrElse("0"));
        redirectSession(req, response, generateSession(default_scenarios[scenario_index]));
    }

    void repeatScenario(session_state& s, const Http::Request& req, Http::ResponseWriter& response) {
        // extract current scenario
        auto scen = s.current_scenario;

        // recreate using same scenario
        string new_session_id = generateSession(scen);
        redirectSession(req, response, new_session_id);
    }

    void newMenu(const Http::Request& req, Http::ResponseWriter& response) {
        kainjow::mustache::mustache tmpl(bubsim::content::indexTemplate);
        kainjow::mustache::data d;

        d.set("running", false);
        d.set("show_graphs", false);
        d.set("show_objectives", false);
        d.set("new_session", true);
        d.set("hide_params", true);
        if (!cfg.imprint.empty()) {
            d.set("imprint", cfg.imprint);
        }

        d.set("version", BUBSIM_VERSION);
        d.set("years_per_round", to_string(BUBSIM_YEARS_PER_ROUND));

        d.set("default_params", toMstch(defaultParameters));

        kainjow::mustache::data scenarios{kainjow::mustache::data::type::list};

        for (int i = 0; i < default_scenarios.size(); i++) {
            kainjow::mustache::data s;
            s = toMstch(default_scenarios[i]);
            s.set("index", to_string(i));
            s.set("first", 0 == i);
            scenarios << s;
        }

        d.set("scenarios", scenarios);
        
        string rendered = tmpl.render(d);

        response.send(Code::Ok, rendered, MIME(Text, Html));
        return;
    }

    void showAllRedeemcodes(const Http::Request& req, Http::ResponseWriter& response) {
        if (cfg.displayredeemcodessecret.empty()
            || !req.query().has("s")
            || req.query().get("s").get() != cfg.displayredeemcodessecret) {
            response.send(Code::Forbidden, "no.", MIME(Text, Plain));
            return;
        }

        string all_codes;

        if (cfg.allcodesfile.empty()) {
            // load from ram
            for (const auto& it : all_redeemcodes) {
                for (const auto& code : it.second) {
                    all_codes += it.first.rc3id + ":" + code + "\n";
                }
            }
        } else {
            // load from file
            ifstream codesfile(cfg.allcodesfile);
            if (!codesfile.is_open()) {
                response.send(Code::Internal_Server_Error, "file read failed", MIME(Text, Plain));
                return;
            }

            all_codes.assign(istreambuf_iterator<char>(codesfile), istreambuf_iterator<char>());
        }
        
        response.send(Code::Ok, all_codes, MIME(Text, Plain));
    }

    void doSth(session_state& s, const Http::Request& req, Http::ResponseWriter& response) {
        // perform action when any (normal) btn on page is pressed

        // update prices
        updatePrices(s, req, response);

        // (maybe) produce a batch
        produceBatch(s, req, response);

        if ("yes" == urlDecode(req.query().get("develop").getOrElse(""))) {
            developBlueprint(s, req, response);
        }

        if ("yes" == urlDecode(req.query().get("advanceyear").getOrElse(""))) {
            advanceYear(s, req, response);
        }

        // return user to main page
        redirectSession(req, response, s.session_id);
    }

    void serveStaticContent(const Http::Request& req, Http::ResponseWriter& response, const string& content) {
        vector<Http::CacheDirective> directives;
        directives.push_back(Http::CacheDirective::Public);
        
        Http::CacheDirective d(Http::CacheDirective::MaxAge, chrono::seconds(1800));
        directives.push_back(d);

        response.headers().add(make_shared<Header::CacheControl>(directives));

        const regex svg_path("[.]svg$");

        const vector<pair<regex, string>> mime_by_pattern = {
            {regex("[.]svg$"), "image/svg+xml"},
            {regex("[.]css$"), "text/css"},
            {regex("[.]ico$"), "image/x-icon"},
        };

        for (const auto& it : mime_by_pattern) {
            if (regex_search(req.resource(), it.first)) {
                response.headers().add(make_shared<Header::ContentType>(it.second));
            }
        }

        response.send(Code::Ok, content);
    }
    
    const regex getUriRegex() const {
        return regex("^/+([^/]*)(?:/+([^/]*))?");
    }

    bool tryParseUri(const string& s) {
        smatch m;
        return regex_search(s, m, getUriRegex());
    }

    string getSessionId(const Http::Request& req) {
        smatch m;
        string s = req.resource();
        if (regex_search(s, m, getUriRegex())) {
            return m[1];
        }

        return "";
    }

    bool tryPageStatic(const Http::Request& req, Http::ResponseWriter& resp) {
        const map<string, string> static_pages = {
            {"/favicon.ico", bubsim::content::favicon},
            {"/style.css", bubsim::content::style},
            {"/ionicons.css", bubsim::content::iconsCss},
            {"/ionicons.eot", bubsim::content::iconsEot},
            {"/ionicons.woff2", bubsim::content::iconsWoff2},
            {"/ionicons.woff", bubsim::content::iconsWoff},
            {"/ionicons.ttf", bubsim::content::iconsTtf},
            {"/binaermensch.svg", bubsim::content::badgeBinaermensch},
            {"/laptopberg.svg", bubsim::content::badgeLaptopberg},
            {"/laptopkopf.svg", bubsim::content::badgeLaptopkopf},
            {"/schroete.svg", bubsim::content::badgeSchroete},
        };

        if (static_pages.find(req.resource()) != static_pages.end()) {
            serveStaticContent(req, resp, static_pages.at(req.resource()));
            return true;
        }

        return false;
    }

    bool tryPageGlobal(const Http::Request& req, Http::ResponseWriter& resp) {
        const map<string, function<void(BubSimHandler&, const Http::Request&, Http::ResponseWriter&)>> global_pages = {
            {"/newsession", &BubSimHandler::newSession},
            {"/newcustomsession", &BubSimHandler::newCustomSession},
            {"/", &BubSimHandler::newMenu},
            {"/new", &BubSimHandler::newMenu},
            {"/allcodes", &BubSimHandler::showAllRedeemcodes},
        };

        if (global_pages.find(req.resource()) != global_pages.end()) {
            global_pages.at(req.resource())(*this, req, resp);
            return true;
        }

        return false;
    }

    bool tryPageSession(session_state& session_ptr, const string& path, const Http::Request& req, Http::ResponseWriter& resp) {
        const map<string, function<void(BubSimHandler&, session_state&, const Http::Request&, Http::ResponseWriter&)>> session_pages = {
            {"advanceyear", &BubSimHandler::advanceYear},
            {"again", &BubSimHandler::repeatScenario},
            {"dosth", &BubSimHandler::doSth},
            {"main", &BubSimHandler::index},
            {"redeemcodes", &BubSimHandler::generateObjectiveRedeemcodes},
        };

        if (session_pages.find(path) != session_pages.end()) {
            session_pages.at(path)(*this, session_ptr, req, resp);
            return true;
        }
        
        return false;
    }

    void onRequest(const Http::Request& req, Http::ResponseWriter response) override {
        if (!tryParseUri(req.resource())) {
            response.send(Code::Internal_Server_Error, "couldn't parse URI");
            return;
        }

        // try to serve a global/static page
        if (tryPageGlobal(req, response) || tryPageStatic(req, response)) {
            return;
        }

        // check if session exists -> if not, send to menu
        if (!sessionExists(getSessionId(req))) {
            response.headers().add(make_shared<Header::Location>("/new"));
            response.send(Code::See_Other);
            return;
        }

        // try to serve a page bound to a session
        smatch m;
        string resource = req.resource();
        regex_search(resource, m, getUriRegex());

        if (!m[2].matched) {
            response.send(Code::Not_Found, "page not found");
            return;
        }

        if (tryPageSession(sessions[getSessionId(req)], m[2], req, response)) {
            return;
        }

        response.send(Code::Not_Found, "page not found");
    }
};

mutex stop_server_mutex;

void signal_handler(int sig) {
    stop_server_mutex.unlock();
    std::exit(sig);
}

int main(int argc, char** argv) {
    cfg.loadFromFile();

    if (argc >= 2) {
        cfg.loadFromFile(argv[1]);
    }

    Address addr(Ipv4::any(), Port(cfg.port));

    Http::Endpoint server(addr);
    auto opts = Http::Endpoint::options()
        .flags(Tcp::Options::ReuseAddr)
        .threads(1);
    server.init(opts);

    stop_server_mutex.lock();
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    server.setHandler(Http::make_handler<BubSimHandler>());
    server.serveThreaded();

    stop_server_mutex.lock();

    server.shutdown();

    return 0;
}
