# automatically calculates patches since last major.minor version
set(GIT_DESCRIBE_SUCCESS FALSE)
find_package(Git)
if(GIT_FOUND)
    execute_process(COMMAND ${GIT_EXECUTABLE} "describe" "--tags" "--match" "v${PROJECT_VERSION}"
                    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                    OUTPUT_VARIABLE PROJECT_VERSION
                    RESULT_VARIABLE GIT_COMMIT_DESCRIBE_RETURN_VAR
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    ERROR_QUIET)

    if ("0" EQUAL "${GIT_COMMIT_DESCRIBE_RETURN_VAR}")
        set(GIT_DESCRIBE_SUCCESS TRUE)
        string(SUBSTRING "${PROJECT_VERSION}" 1 -1 PROJECT_VERSION)
    endif()
endif()
