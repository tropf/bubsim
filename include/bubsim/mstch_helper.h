/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_MSTCH_HELPER_H_INCLUDED__
#define __BUBSIM_MSTCH_HELPER_H_INCLUDED__

#include <vector>
#include <map>
#include <string>

#include <mustache.hpp>

#include <bubsim/state.h>
#include <bubsim/model.h>
#include <bubsim/objective.h>
#include <bubsim/params.h>
#include <bubsim/scenario.h>
#include <bubsim/session.h>

using namespace kainjow;

namespace bubsim {
    mustache::data toMstch(const globalStats& s);
    mustache::data toMstch(const model& m);
    mustache::data toMstch(const objective& o, const state& s, const std::map<objective, std::string>& redeem_codes = {}, const std::string& redeemurl_pattern = "");
    mustache::data toMstch(const std::vector<objective>& o, const state& s, const std::map<objective, std::string>& redeem_codes = {}, const std::string& redeemurl_pattern = "");
    //mustache::data toMstch(const annual_accounts& acc);
    mustache::data toMstch(const simulationParameters& p);
    mustache::data toMstch(const scenario& s);
    mustache::data toMstch(const annual_accounts& acc);
}

#endif // __BUBSIM_MSTCH_HELPER_H_INCLUDED__
