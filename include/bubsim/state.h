/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_STATE_H_INCLUDED__
#define __BUBSIM_STATE_H_INCLUDED__

#include <string>
#include <vector>
#include <functional>
#include <map>

#include <bubsim/model.h>
#include <bubsim/params.h>
#include <bubsim/marketcell.h>

using std::string;
using std::vector;
using std::map;

namespace bubsim {
    struct maximums {
        int cost = 0;
        int gflops = 0;
        int existingCnt = 0;
    };
    typedef struct maximums maximums;

    struct globalStats {
        int productionCost = 0;
        int productionCnt = 0;
        int salesEarnings = 0;
        int salesCnt = 0;
        int warrantyCost = 0;
        int warrantyCnt = 0;
        int blueprintCost = 0;
        int blueprintCnt = 0;
        int thrownOutPerformanceCnt = 0;
        int brokenCnt = 0;
        int totalUsageYears = 0;
        int suppliedCnt = 0;
        int suppliedNotCnt = 0;

        int totalThrownOut() const;
        float avgUsage() const;
    };
    typedef struct globalStats globalStats;

    class state {
    public:
        int money;
        int year;
        vector<model> blueprints;
        map<model, int> stock;
        map<model, int> pricing;
        vector<marketCell> market;
        map<model, int> sales;
        map<model, int> warrantyCases;
        simulationParameters params;

        globalStats stats;

        /// maximum values
        struct maximums maxs;

        state(simulationParameters givenParams = defaultParameters);

        /**
         * update the maximum values
         */
        void updateMaxs();

        /**
         * advances the year by one and simulates sales for that year
         */
        void advanceYear();
        
        /**
         * @return whether blueprint can be developed
         */
        bool canDevelopBlueprint();

        /**
         * Develops a new blueprint
         * @param focus_cost [0.0, 1.0] how much to focus on low cost
         * @param focus_durability [0.0, 1.0] how much to focus on high durability
         * @param focus_gflops [0.0, 1.0] how much to focus on high gflops (performance)
         * @return state with new blueprint developed
         */
        state developBlueprint(float focus_durability, float focus_gflops);

        /**
         * checks if m can be produced 100 times.
         * checks if m is in known blueprints and if money is sufficient to produce BUBSIM_BATCH_SIZE units
         * @return whether m can be produced BUBSIM_BATCH_SIZE times
         */
        bool canProduce(const model& m);

        /**
         * produce BUBSIM_BATCH_SIZE of model m and return the resulting state
         * @param m model to produce
         * @return state with BUBSIM_BATCH_SIZE produced of the new model
         */
        state produce(const model& m);

        /**
         * check if a model by the given name exists
         * @param name name to check against
         * @return whether model w/ given name exists
         */
        bool modelExists(const string& name);

        /**
         * generates a non-existent name for a model
         * @return unused model name
         */
        string getNewModelName();

        /**
         * retrieve a model from blueprints with the given name
         * @param name model name to search for
         * @return model with the given name
         */
        model getModelByName(const string& name);

        /**
         * sets the price for a particular model
         * @param m model to set the price for
         */
        void setPrice(const model& m, int newprice);

        map<model, int> getAvailableModelPrices();

        int getYear();
        int getMoney();
        vector<model> getBlueprints();
        map<model, int> getStock();
        map<model, int> getPricing();
    };
}

#endif // __BUBSIM_STATE_H_INCLUDED__
