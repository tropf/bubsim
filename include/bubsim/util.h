/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_UTIL_H__
#define __BUBSIM_UTIL_H__

#include <string>

using namespace std;

namespace bubsim {
    string urlEncode(const string &value);
    string urlDecode(const string &SRC);

    /**
     * remove all characters except a-zA-Z0-9 from the given string
     * @param unsafe string containing special chars
     * @return same string without these chars
     */
    string getSafeString(const string& unsafe);

    /**
     * get a random (human-readable) string of chars
     */
    string getRandomString(size_t length = 8);

    /**
     * get a string with whitespaces removed from beginning and end
     * @params s string with whitespaces
     * @return same string w/o leading or trailing spaces
     */
    string getStrippedString(const string& s);
}
#endif // __BUBSIM_UTIL_H__
