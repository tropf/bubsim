/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_CFG_H_INCLUDED__
#define __BUBSIM_CFG_H_INCLUDED__

#include <string>
#include <map>

#include <bubsim/config.h>

using std::string;

namespace bubsim {
    /**
     * contains cfg loaded from cfgfile
     */
    struct runtimeCfg {
        /// raw data as read from file
        std::map<string, string> raw;

        /// imprint to be displayed
        string imprint = "";
        string displayredeemcodessecret = "";
        int port = 9080;
        string allcodesfile = "";
        string redeemurl = "";

        /**
         * load data from file into map & attributes
         */
        void loadFromFile(string path = BUBSIM_RUNTIME_CFG_PATH);

        /*
         * loads attributes from raw map
         */
        void loadAttributesFromRaw();
        
    };
    typedef struct runtimeCfg runtimeCfg;
    
}

#endif // __BUBSIM_CFG_H_INCLUDED__
