/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_SCENARIO_H_INCLUDED__
#define __BUBSIM_SCENARIO_H_INCLUDED__

#include <string>
#include <vector>

#include <bubsim/objective.h>

namespace bubsim {
    struct scenario {
        std::string name;
        std::string description;
        simulationParameters params;
        std::vector<objective> objectives;
        bool tutorial;
    };
    typedef struct scenario scenario;

    bool operator<(const scenario& lhs, const scenario& rhs);
    bool operator==(const scenario& lhs, const scenario& rhs);
    bool operator!=(const scenario& lhs, const scenario& rhs);

    extern const vector<objective> default_objectives;
    extern const vector<scenario> default_scenarios;
}

#include <bubsim/objective.h>

#endif // __BUBSIM_SCENARIO_H_INCLUDED__
