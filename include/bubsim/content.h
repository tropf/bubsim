/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <string>

using namespace std;

namespace bubsim {
    namespace content {
        extern const string indexTemplate;
        extern const string style;
        extern const string iconsCss;
        extern const string iconsEot;
        extern const string iconsWoff;
        extern const string iconsWoff2;
        extern const string iconsTtf;
        extern const string favicon;
        extern const string badgeBinaermensch;
        extern const string badgeLaptopberg;
        extern const string badgeLaptopkopf;
        extern const string badgeSchroete;
    }
}
