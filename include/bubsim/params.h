/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_PARAMS_H_INCLUDED__
#define __BUBSIM_PARAMS_H_INCLUDED__

#include <functional>

namespace bubsim {
    struct simulationParameters {
        int warrantyYears = 1;
        int mooresMonths = 36;
        int avgCustomerAcceptedPrice = 100;
        int developmentCost = 10000;
        int startingMoney = 50000;
        float acceptedPerformanceFractionAvg = 0.7f;

        int currentGflops(int year) const;
    };

    typedef struct simulationParameters simulationParameters;
    const simulationParameters defaultParameters;

    bool operator<(const simulationParameters& lhs, const simulationParameters& rhs);
    bool operator==(const simulationParameters& lhs, const simulationParameters& rhs);
    bool operator!=(const simulationParameters& lhs, const simulationParameters& rhs);
}

namespace std {
    template <> struct hash<bubsim::simulationParameters> {
        size_t operator()(const bubsim::simulationParameters& p) const noexcept {
            auto ihash = hash<int>{};
            auto fhash = hash<float>{};
            return ihash(p.warrantyYears) ^ ihash(p.mooresMonths) ^ ihash(p.avgCustomerAcceptedPrice) ^ ihash(p.developmentCost) ^ ihash(p.startingMoney) ^ fhash(p.acceptedPerformanceFractionAvg);
        }
    };
}

#endif // __BUBSIM_PARAMS_H_INCLUDED__
