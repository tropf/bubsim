/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM__SESSIONS_HANDLER_H_INCLUDED__
#define __BUBSIM__SESSIONS_HANDLER_H_INCLUDED__

#include <map>
#include <string>

#include <bubsim/session.h>
#include <bubsim/state.h>
#include <bubsim/scenario.h>

namespace bubsim {
    class SessionsHandler {
    protected:
        std::map<std::string, session_state> sessions;

    public:
        /**
         * initialize a new session
         * @return the generated sessions id
         */
        string generateSession(scenario scen);

        /**
         * check if given session exists
         * @param session_key session id to check for
         * @return whether session w/ given id exists
         */
        bool sessionExists(string session_key);

        /**
         * rm old sessions
         */
        void cleanupSessions();

        /**
         * get a session with the given key.
         * generated if it does not exist.
         * also update the "last accessed" info for the session
         * @param session_key id of the session to be retrieved
         * @param scen scenario to use when a new session is generated
         * @return a valid session
         */
        session_state getOrCreateSession(std::string session_key, scenario scen = default_scenarios[0]);
    };
}

#endif // __BUBSIM__SESSIONS_HANDLER_H_INCLUDED__
