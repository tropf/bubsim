/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_MODEL_H_INCLUDED__
#define __BUBSIM_MODEL_H_INCLUDED__

#include <string>

namespace bubsim {
    struct model {
        int cost;
        int year;
        std::string name;
        float durability;
        int gflops;
    };
    const model emptyModel = {0, 0, "", 1};
    typedef struct model model;

    bool operator<(const model& lhs, const model& rhs);
    bool operator==(const model& lhs, const model& rhs);
    bool operator!=(const model& lhs, const model& rhs);
}

#endif // __BUBSIM_MODEL_H_INCLUDED__
