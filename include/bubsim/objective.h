/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_OBJECTIVE_H_INCLUDED__
#define __BUBSIM_OBJECTIVE_H_INCLUDED__

#include <functional>
#include <string>

#include <bubsim/state.h>

namespace bubsim {
    struct objective {
        std::string name;
        std::string description;
        std::string rc3id;
        std::string icon;
        /// progress from 0 to 1
        std::function<float(state)> progress;
    };
    typedef struct objective objective;

    bool operator<(const objective& lhs, const objective& rhs);
    bool operator==(const objective& lhs, const objective& rhs);
    bool operator!=(const objective& lhs, const objective& rhs);

    float progressReachTotalSales(state s, int goal);
    float progressReachAvgUsage(state s, float goal);
    float progressReachSupply(state s, float goal);
    float progressReachYear(state s);

    // note define constants here, as function pointers are not resolved from within the implementation file
    const objective objective_tutorial = {
        "Simulationsbediener·in",
        "Schließe das Tutorial-Szenario ab",
        "simulationsbedienerin",
        "/binaermensch.svg",
        bubsim::progressReachYear
    };

    const objective objective_busy = {
        "Geschäftig",
        "Erziele mindestens 300000 Umsatz.",
        "geschaeftig",
        "/laptopberg.svg",
        std::bind(bubsim::progressReachTotalSales, std::placeholders::_1, 300000)
    };

    const objective objective_usage = {
        "Langlebig",
        "Erreiche im Schnitt eine Lebensdauer von 3,5 Jahren pro Laptop.",
        "langlebig",
        "/schroete.svg",
        std::bind(bubsim::progressReachAvgUsage, std::placeholders::_1, 3.5f)
    };

    const objective objective_supply = {
        "Laptops für alle!",
        "Versorge im Schnitt 30% der Kund·innen mit einen Laptop.",
        "laptopsfueralle",
        "/laptopkopf.svg",
        std::bind(bubsim::progressReachSupply, std::placeholders::_1, 0.3)
    };
}

namespace std {
    template <> struct hash<bubsim::objective> {
        size_t operator()(const bubsim::objective o) const noexcept {
            auto shash = std::hash<string>{};
            return shash(o.name) ^ shash(o.description) ^ shash(o.rc3id) ^ shash(o.icon);
        }
    };

    template <> struct hash<vector<bubsim::objective>> {
        size_t operator()(const vector<bubsim::objective>& objectives) const noexcept {
            auto ohash = std::hash<bubsim::objective>{};
            size_t acc = 0;

            for (const auto& o : objectives) {
                acc ^= ohash(o);
            }

            return acc;
        }
    };
}

#endif // __BUBSIM_OBJECTIVE_H_INCLUDED__
