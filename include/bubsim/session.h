/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_SESSION_H_INCLUDED__
#define __BUBSIM_SESSION_H_INCLUDED__

#include <string>
#include <chrono>
#include <map>
#include <list>

#include <bubsim/state.h>
#include <bubsim/scenario.h>
#include <bubsim/objective.h>

using namespace std::chrono;

namespace bubsim {
    struct annual_accounts {
        int sales;
        int development;
        int production;
        int warranty;
        int year;

        int total_revenue() const;
        int total_expenses() const;
    };
    typedef struct annual_accounts annual_accounts;

    struct session_state {
        std::string session_id;
        time_point<steady_clock> last_seen;
        state current_state;
        scenario current_scenario;
        annual_accounts accounts;
        globalStats old_stats;
        bool restore_scroll_next_load = false;
        bool stored_for_comparison = false;
        map<objective, string> redeem_codes;

        session_state(scenario scen = default_scenarios[0]) : current_scenario(scen), current_state(scen.params) {}

        /**
         * returns true if scroll position should be kept for the next load & then resets restore_scroll_next_load
         */
        bool should_restore_scroll();

        /**
         * store this session for comparison if it has not been stored yet and is ready to be stored
         */
        void maybeStoreForComparison(std::map<scenario, std::list<globalStats>>& storage);

        /**
         * returns true iff the there are less rounds than bubsim_max_year
         */
        bool isRunning();

        /**
         * get redeem codes for all completed objectives **only iff the game is not running anymore**
         */
        void getObjectiveRedeemCodes(const map<string, string>& cfg);
    };
    typedef struct session_state session_state;
}

#endif // __BUBSIM_SESSION_H_INCLUDED__
