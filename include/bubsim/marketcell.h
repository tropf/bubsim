/* Copyright (C) 2019, 2020 Rolf Pfeffertal
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#ifndef __BUBSIM_MARKETCELL_H_INCLUDED__
#define __BUBSIM_MARKETCELL_H_INCLUDED__

#include <map>

#include <bubsim/model.h>
#include <bubsim/params.h>

namespace bubsim {
    enum throwingOutReason {
                            performance,
                            broken,
                            keep,
                            hadNone,
    };

    struct marketCell {
        /// current used model
        model currentModel = emptyModel;

        /// max age an entity accepts of their product
        int maxAge;
        
        /// max price the entity is willing to pay
        int maxPrice;

        /// minimum performance (compared to the usual performance) the entity is willing to accept
        float minPerformance;

        /// year, the device has been bought
        int boughtYear = -100;

        /// price the model has been bought for
        int boughtPrice = 0;

        simulationParameters params;

        marketCell(simulationParameters givenParams = defaultParameters);

        /**
         * simulate a passing year:
         * throw out breaking devices
         * throw out too old devices
         */
        throwingOutReason advanceYear(int targetYear);

        /**
         * pick a model from a selection of models
         */
        model pickModel(int currentYear, std::map<model, int> priceByModel);

        /**
         * @return the currently expected performance
         */
        int expectedPerformance(int year);
    };
}


#endif // __BUBSIM_MARKETCELL_H_INCLUDED__
